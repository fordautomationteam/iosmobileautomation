package TestCase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.touch.TouchActions;

import com.ford.automation.CommonAPI.BasicAPI;

public class Case extends BasicAPI implements Runnable{
/******************************************************************************************************************************************
 *                                        Test Data Configuration
 *****************************************************************************************************************************************/
	public String USERNAME="tlccz@163.com";    
	public String PASSWORD="password";                 
	public String FINALAUTHVEHICEL="Oh to be 15";           
	public String INITIALAUTHVEHICEL="";
	public String NONEAUTHVEHICLE="";
	public String INACTIVMSG="Your account is not active. To confirm your account, click the link in the email sent during registration. Sometimes delivery takes 5 minutes or longer. If you don't see the message, make sure your email filtering options allow messages from no-reply@myfordmobile.com.";
	public String BEVVEHICLE="oh to be 15";
	public String PHEVVEHICLE="Golden Cmax";
	public String SINGLEACCOUNT="binliu.singlevin@gmail.com";
	public String NONEVINACCOUNT="binliu.ford@yahoo.com";
	public String ADDACCOUNT="binliu.ford@yahoo.com";

	// Vehicle Information
	public String ODOMETER = "95697";
	public String BATTERYHELATH = "poor";
	public String TIREPRESSURE = "poor";
	public String BATTERYPERFORMANCE = "good";
	public String MAINTENANCE = "No upcoming maintenance";
	
	
/******************************************************************************************************************************************
 *                                        Common Function Define At Here
 *****************************************************************************************************************************************/
	void logInNormal(String UserName,String Password,boolean RememeberEnable,boolean PasscodeEnable)
	{
		delay(5000);
		if(buttonExits("Set Passcode","One",false))
		{
			buttonClick("Set Passcode","One",true);
			buttonClick("Set Passcode","Two",true);
			buttonClick("Set Passcode","Three",true);
			buttonClick("Set Passcode","Four",true);
		}
		if(buttonExits("Log In","Mfm logo",false))
		{
			inputBoxWrite("Log In","Email Address",UserName,true);
			inputBoxWrite("Log In","Password",Password,true);
			if(RememeberEnable)
			{
				buttonClick("Log In","Rememeber me",true);
				buttonClick("Log In","Sign In",true);
				if(PasscodeEnable)
				{
					alertCheck("Set Passcode",true);
					delay(2000);
					buttonClick("Set Passcode","One",true);
					buttonClick("Set Passcode","Two",true);
					buttonClick("Set Passcode","Three",true);
					buttonClick("Set Passcode","Four",true);
					buttonClick("Set Passcode","One",true);
					buttonClick("Set Passcode","Two",true);
					buttonClick("Set Passcode","Three",true);
					buttonClick("Set Passcode","Four",true);
				}
				else
				{
					alertCheck("No Passcode",true);
					alertCheck("Yes",true);
				}
			}
			else
				buttonClick("Log In","Sign In",true);
		}
		if(alertExits(false))
			alertCheck("Yes",true);
		buttonExits("Home Screen","Menu Button",60,true);
	}
	
	void logOut()
	{
		if(buttonExits("Navigation Page", "Account",false))
		{
			buttonClick("Navigation Page", "Account",true);
			delay(2000);
			buttonClick("Account Page", "Security",true);
			delay(2000);
			buttonClick("Security Page", "Log Out",true);
			delay(5000);
		}
	}
	
	void testhelp_selectveh(String VehicleName)
	{
		String currentVehicle = buttonGetText("Remote Control", "Current Vehicle",true);
		if(currentVehicle.equals(VehicleName) != true)
		{
			buttonClick("Remote Control", "Current Vehicle",true);
			delay(2000);
			clickListByText("Vehicle Select","Vehicle Option",VehicleName,true);
			delay(5000);
		}
			
	}
	
	void MyVehicle_selecVeh(String NickName)
	{
		WebElement textElement = findElemByName(TestDriver,NickName);
		if(textElement!=null)
		{
			updateLogText("The vehicle can be founded!");
			while(!textElement.isDisplayed())
			{
				swipe(0.5,0.6,0.5,0.45);
				textElement = findElemByName(TestDriver,NickName);
				updateLogText("Swipe Screen!\n");
			}
			updateLogText("The vehicle can be Displayed!");
			textElement.click();
			delay(5000);
		}
	}
		
	
/****************************************************************************************************************************************** 
 *                                            Test Case define here
 *****************************************************************************************************************************************/	
	void tescase_ChangePassword(String CaseCode)
	{
		startTestCase("Account","Modify Account Info",CaseCode);
		logInNormal(USERNAME,PASSWORD,false,false);
		buttonClick("Home Screen","Menu Button",true);
		buttonClick("Menu Screen","Account",true);
		textBoxCompareText("Account","Title","Account",15,true);
		WebElement ChangeBtn = findElemByXPath(TestDriver,"//*[@id='current']/div/div[2]/div[3]/button[1]");
		if(ChangeBtn.isDisplayed())
		{
			System.out.println("Change Password Button displayed!");
		}
		if(ChangeBtn!=null)
		{
			TouchActions swipe = new TouchActions(TestDriver).flick(ChangeBtn, 0, -300, 1);
	    	swipe.perform();
		}
		delay(10000);
		buttonClick("Account","Change Password",true);
		textBoxCompareText("Change Password","Title","Password",15,true);
		inputBoxWrite("Change Password","New Password","PASSWORD",true);
		inputBoxWrite("Change Password","Confirm Password","PASSWORD",true);
		buttonClick("Change Password","Save",true);
		delay(10000);
		stopTestCase("Account","Modify Account Info",CaseCode);
	}
	
/******************************************************************************************************************************************
 *                                            Test Case define here
 *****************************************************************************************************************************************/	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		//testcase_LogIn("2-E-18");
		tescase_ChangePassword("2-E-15");
		//testcase_LogOut("2-E-17");
		stopTest();
	}
}

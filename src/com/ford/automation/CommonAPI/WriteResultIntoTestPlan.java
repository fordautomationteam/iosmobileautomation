package com.ford.automation.CommonAPI;

import java.io.*;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.*;


public class WriteResultIntoTestPlan extends SeleniumCommonAPI {

	// The test case unique code
	private String TestCaseCode;
	
	// Test result
	private String TestResult;
	
	// Sheet number
	private int SheetNum;
	
	// Column number
	private int ColumnNum;
	
	// Row number
	private int RowNum;
	
	//Test plan work book
	private HSSFWorkbook TestPlanWb; 
	
	//Test plan work sheet
	private HSSFSheet TestPlanWs;
	
	//Modify cell
	private Cell ModifyCell;

	// Write the test result into the MFM Smoke test plan
	public WriteResultIntoTestPlan(String aTestCaseCode,String aTestResult)
	{
		this.TestCaseCode = aTestCaseCode;
		this.TestResult = aTestResult;
		System.out.println("Testcasecode is:" + TestCaseCode +" and testResult is: "+TestResult);
	}	
	
	/**
	 * Modify the test result into the test plan 
	 * @throws IOException
	 */
	public void modifyTestPlan() throws IOException
	{
		String resultPath=System.getProperty("user.dir")+"/TestResults/TestPlanResult.xls";
		FileInputStream input_document = null;
		System.out.println("Open the Test Plan:"+resultPath);
		if(FirstTimeOpenTestPlan)
		{
			System.out.println("Open the Original Test Plan:"+TestPlanPath);
			//Read the spreadsheet that needs to be updated
	        input_document = new FileInputStream(new File(TestPlanPath));
	        FirstTimeOpenTestPlan = false;
		}
		else
		{
			System.out.println("Open the Current Test Plan:"+resultPath);
			input_document = new FileInputStream(new File(resultPath));
		}
		System.out.println("Writing Process!");
		;
        //Access the workbook
        TestPlanWb = new HSSFWorkbook(input_document); 
        input_document.close();
        String[] TestCaseCodesArray = TestCaseCode.split(",");
        System.out.println("TestCaseCode="+TestCaseCode+" TestCaseCodesArray:"+TestCaseCodesArray.length);
        for(int i=0;i<TestCaseCodesArray.length;i++)
        {
        	String[] TestCaseCodeArray = TestCaseCodesArray[i].split("-");
        	SheetNum = Integer.parseInt(TestCaseCodeArray[0])-1;
    		ColumnNum = TestCaseCodeArray[1].charAt(0) - 65;
    		RowNum = Integer.parseInt(TestCaseCodeArray[2]) - 1;
    		System.out.println("Modified the Cell value!with"+SheetNum+"Clumn:"+ColumnNum+"RowNum:"+RowNum);
            //Access the worksheet, so that we can update / modify it.
    		TestPlanWs = TestPlanWb.getSheetAt(SheetNum);
            modifyTestResult(TestPlanWs);
        }
        //Close the InputStream
        saveAndClosed();
	}
	
	public void saveAndClosed() throws IOException
	{
    	String resultPath=System.getProperty("user.dir")+"/TestResults/TestPlanResult.xls";
    	System.out.println("the save path is:   "+resultPath);
        //Open FileOutputStream to write updates
        FileOutputStream output_file =new FileOutputStream(new File(resultPath));
        //write changes
        TestPlanWb.write(output_file);
        //close the stream
        output_file.close();
        TestPlanWb.close();
	}
		
	
	public void modifyTestResult(HSSFSheet Ws)
	{
		if(TestCaseCode != null)
		{
			ModifyCell = Ws.getRow(RowNum).getCell(ColumnNum);
			ModifyCell.setCellValue(TestResult);
			System.out.println("Modified the Cell value!");
		}
	}
}

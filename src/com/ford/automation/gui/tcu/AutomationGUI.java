package com.ford.automation.gui.tcu;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Button;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import javax.tools.Diagnostic;
import javax.tools.DiagnosticListener;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.StandardLocation;
import javax.tools.ToolProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.wb.swt.SWTResourceManager;

import com.ford.automation.CommonAPI.GlobalParameters;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.ModifyEvent;

public class AutomationGUI extends GlobalParameters{

	protected Shell shlAutomationTool;
	private Text textScriptPath;
	private Button btnIos;
	private Button btnAndroid;
	protected static StyledText logStatus;
	public static Display display;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			AutomationGUI window = new AutomationGUI();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		display = Display.getDefault();
		createContents();
		shlAutomationTool.open();
		shlAutomationTool.layout();
		while (!shlAutomationTool.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		
		String workingDir = System.getProperty("user.dir");
		String IconPath=workingDir+"/ford.ico";
		shlAutomationTool = new Shell();
		shlAutomationTool.setImage(SWTResourceManager.getImage(IconPath));
		shlAutomationTool.setSize(450, 300);
		shlAutomationTool.setText("Automation GUI");
		
		Group group = new Group(shlAutomationTool, SWT.NONE);
		group.setBounds(27, 10, 402, 258);
		
		Button buttonScriptPath = new Button(group, SWT.NONE);
		buttonScriptPath.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String ScriptPath = getObjectPathFromFileSystem();
				if(ScriptPath != null)
				{
					textScriptPath.setText(ScriptPath);
				}
			}
		});
		buttonScriptPath.setText("...");
		buttonScriptPath.setBounds(348, 44, 40, 28);
		
		textScriptPath = new Text(group, SWT.BORDER);
		textScriptPath.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				if(textScriptPath.getText() != null)
				{
					ScriptPath = textScriptPath.getText();
					File Source = new File(ScriptPath);
					File Dest = new File(System.getProperty("user.dir")+"/src/TestCase/Case.java");
					copyFileUsingFileStreams(Source,Dest);
				}
			}
		});
		textScriptPath.setBounds(82, 48, 260, 19);
		Label lbScriptPath = new Label(group, SWT.NONE);
		lbScriptPath.setBounds(10, 51, 66, 14);
		lbScriptPath.setText("Script Path:");
		
		Button btnRunScript = new Button(group, SWT.NONE);
		btnRunScript.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(btnIos.getSelection())
				{
					PhoneOS = "IOS";
				}
				if(btnAndroid.getSelection())
				{
					PhoneOS = "Android";
				}
				writePhoneOS();
				AutoToolThread = new Thread(){
					public void run()
					{
						String[] commandResult=new String[64];
						String workingDir = System.getProperty("user.dir");
						String Commands="java -jar -XstartOnFirstThread /Users/bliu52/Documents/workspace/FordPassAutomation/AutomationTool.jar";
						runWinCommnad(Commands,commandResult);
					}
				};
				AutoToolThread.start();
			}
		});
		btnRunScript.setBounds(285, 206, 93, 38);
		btnRunScript.setText("Open Tool");
		
		logStatus = new StyledText(group, SWT.V_SCROLL | SWT.BORDER | SWT.H_SCROLL);
		logStatus.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				logStatus.setTopIndex(logStatus.getLineCount() - 1);
			}
		});
		logStatus.setBounds(10, 71, 358, 129);
		
		
		Button btnCompile = new Button(group, SWT.NONE);
		btnCompile.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					complieTestCases();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				String[] commandResult=new String[64];
				String Commands="jar uf ./AutomationTool.jar ./TestCase/Case.class";
				updateRunningTestCase(Commands);
				runWinCommnad(Commands,commandResult);
					
			}
		});
		btnCompile.setBounds(10, 206, 84, 38);
		btnCompile.setText("Compile");
		
		btnAndroid = new Button(group, SWT.RADIO);
		btnAndroid.setFont(SWTResourceManager.getFont("Apple Braille", 25, SWT.NORMAL));
		btnAndroid.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				PhoneOS = "Android";
			}
		});
		btnAndroid.setBounds(58, 10, 120, 28);
		btnAndroid.setText("Android");
		
		btnIos = new Button(group, SWT.BORDER | SWT.RADIO);
		btnIos.setSelection(true);
		btnIos.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				PhoneOS = "IOS";
			}
		});
		btnIos.setFont(SWTResourceManager.getFont("Apple Braille", 25, SWT.NORMAL));
		btnIos.setBounds(246, 10, 76, 28);
		btnIos.setText("IOS");
	}
	
	/**
	 * Get the object map files path
	 */
	protected String getObjectPathFromFileSystem()
	{
		FileDialog fd = new FileDialog(shlAutomationTool, SWT.OPEN);
        fd.setText("Open");
        fd.setFilterPath("C:/");
        String[] filterExt = { "*.java"};
        fd.setFilterExtensions(filterExt);
        String selected = fd.open();
        return selected;
	}
	
	/**
	 * Running windows command
	 */
	public static int runWinCommnad(String Commands,String[] commandResult)
	{
		Runtime rt = Runtime.getRuntime();
		Process proc = null;
		int line=0;
		try {
			proc = rt.exec(Commands);
		} 
		catch (IOException e) {
			e.printStackTrace();
		}

		BufferedReader stdInput = new BufferedReader(new 
		     InputStreamReader(proc.getInputStream()));
		// read the output from the command
		String s = null;
		try {
			while ((s = stdInput.readLine()) != null && line<1024*64) {
				commandResult[line]=s;
				line++;
				updateRunningTestCase(s+"\n");
			}
		} 
		
		catch (IOException e1) 
		{
			e1.printStackTrace();
		}
		return line;
	}
	
	/**
	 * Add compile diagnostic listener
	 * @author BLIU52
	 *
	 */
	public static class MyDiagnosticListener implements DiagnosticListener<JavaFileObject>
    {
        public void report(Diagnostic<? extends JavaFileObject> diagnostic)
        {
 
            System.out.println("Line Number->" + diagnostic.getLineNumber());
            System.out.println("code->" + diagnostic.getCode());
            System.out.println("Message->"
                               + diagnostic.getMessage(Locale.ENGLISH));
            System.out.println("Source->" + diagnostic.getSource());
            System.out.println(" ");
        }
    }
	
	
	public static boolean compileFile(File sourceFile) throws IOException
	{
		String workingDir = System.getProperty("user.dir");
		
		//get system compiler:
		System.out.println("java.home = " + System.getProperty("java.home")+"\n");
		//System.setProperty ( "java.home","JDK jre directory" );
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        MyDiagnosticListener c = new MyDiagnosticListener();
        boolean success=false;
        
        try{
	        StandardJavaFileManager fileManager = compiler.getStandardFileManager(c,
	              Locale.ENGLISH,
	              null);
		    fileManager.setLocation(StandardLocation.CLASS_OUTPUT, Arrays
		          .asList(new File(workingDir)));
		    
		    updateRunningTestCase("Step 2 \n");
		    // Compile the file
		    success = compiler.getTask(null, fileManager, null, null, null,
		    fileManager.getJavaFileObjectsFromFiles(Arrays.asList(sourceFile)))
		          .call();
		    updateRunningTestCase("Step 3 \n");
		    fileManager.close();
        }
        catch(NullPointerException e)
        {
        	updateRunningTestCase(e.toString()+"\n");
        }
        
	    return success;
	}
	
	
	/**
	 * 
	 * Compile all test case code
	 * @throws Exception
	 */
	public static void complieTestCases() throws Exception {
		String workingDir = System.getProperty("user.dir");
		File folder = new File(workingDir+"//src");
		File[] listOfFiles = folder.listFiles();
      
		for (int i = 0; i < listOfFiles.length; i++) 
		{
		  System.out.println("Directory " + listOfFiles[i].getName());
    	  if(listOfFiles[i].isDirectory())
    	  {
    		  if(listOfFiles[i].getName().equals("TestCase"))
    		  {
    			  System.out.println("Directory " + listOfFiles[i].getName());
    			  File[] listTestCases = listOfFiles[i].listFiles();
    			  for (int j = 0; j < listTestCases.length; j++) 
    		      {
			          if (listTestCases[j].isFile()) 
			          {
			        	  System.out.println("	File " + listTestCases[j].getAbsolutePath());
			        	  updateRunningTestCase(listTestCases[j].getAbsolutePath()+"\n");
			        	  File sourceFile=listTestCases[j];
			        	  compileFile(sourceFile);
			          }
    		      }
    		  }
    	  }
		}
	}
	
	private static void copyFileUsingFileStreams(File source, File dest) 
	{
	    InputStream input = null;
	    FileOutputStream output = null;
        try 
        {
			input = new FileInputStream(source);
			output = new FileOutputStream(dest);
	        byte[] buf = new byte[1024];
	        int bytesRead;
	        while ((bytesRead = input.read(buf)) > 0) {
	            output.write(buf, 0, bytesRead);
	        }
	        input.close();
	        output.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	/**
	 * This function is used to collect the configuration data from the GUI
	 */
	protected void writePhoneOS()
	{
		try {
			// Save the programmer logging file
			boolean PhoneOSExist = false;
			File dir = new File("Cfg");
			if(dir.exists() == false)
				dir.mkdir();
			ArrayList<String> lines = new ArrayList<String>();
	        String line = null;
			String fileName = System.getProperty("user.dir")+"/Cfg/Config.txt";
			File LogFile=new File(fileName);
			if(LogFile.exists() == false)
			{
				LogFile.createNewFile();
			}
			
			FileReader fr = new FileReader(LogFile);
            BufferedReader br = new BufferedReader(fr);
            
            while ((line = br.readLine()) != null) {
                if (line.contains("PhoneOS"))
                {
                	line = "PhoneOS="+PhoneOS;
                	PhoneOSExist = true;
                }
                System.out.println(line);    
                lines.add(line);
            } 
            if(PhoneOSExist == false)
            {
            	lines.add("PhoneOS="+PhoneOS);
            }
            fr.close();
            br.close();

            FileWriter fw = new FileWriter(LogFile);
            BufferedWriter bw = new BufferedWriter(fw);
            for(String s : lines)
            	bw.write(s+"\n");
            bw.flush();
            bw.close();
            
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	/**
    *
     * @param statusStr: the status string need update on the GUI
    */
    public synchronized static void updateRunningTestCase(final String TextDisplay)
	{
	     if (com.ford.automation.gui.tcu.AutomationGUI.display == null || com.ford.automation.gui.tcu.AutomationGUI.display.isDisposed())
	         return;
	     com.ford.automation.gui.tcu.AutomationGUI.display.asyncExec(new Runnable() {
	
	         public void run() {
	           com.ford.automation.gui.tcu.AutomationGUI.logStatus.append(TextDisplay);
	         }
	     });
	}
}



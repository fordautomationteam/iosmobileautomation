package com.ford.automation.gui.tcu;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Group;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JOptionPane;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Text;
import com.ford.automation.CommonAPI.*;

import TestCase.Case;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;


public class AutomationTool extends BasicAPI {

	protected Shell shlAutomationTool;
	private Text textAppPath;
	private Text textObjMapPath;
	private Thread JuythonThread = null;
	static public Object ThreadSync = new Object();
	static private boolean bGUIdone = false;
	static private String m_AndroidVersion = "";
	static private String m_AndroidDevice = "";
	static private String m_AppPath = "";
	// log print text object
	public static StyledText styledTextLog;
	public static StyledText styledAppiumLog;
	public static Display display;
	private Text textAppVersion;
	private Combo comboSheetName;
	private Combo comboDeviceID;
	private Text textTestPlanPath;
	public static Text textGroup;
	public static Text textFeature;
	
	public String getDeviceName()
	{
		return m_AndroidDevice;
	}
	
	public String getAppPath()
	{
		return m_AppPath;
	}
	
	public String getAndroidVersion()
	{
		return m_AndroidVersion;
	}
	
	public void runThreadTest()
	{
		(new Thread(new exitEvent())).start();
	}

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			AutomationTool window = new AutomationTool();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void startGUI()
	{
		JuythonThread = new Thread(){
			public void run()
			{
				GUIStart();
			}
		
		};
		JuythonThread.start();
		//Wait for the signal from the GUI thread that the command is done
		synchronized(ThreadSync)
		{
		
			if (bGUIdone == false)
			{
				System.out.println(" the Gui Done value ");
				System.out.println(bGUIdone);
				try {
					ThreadSync.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
	}
	private void  GUIStart()
	{
		System.out.println("From new thread");
		
		try {
			AutomationTool window = new AutomationTool();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * Open the window.
	 */
	public void open() {
		display = Display.getDefault();
		createContents();
		shlAutomationTool.open();
		shlAutomationTool.layout();
		while (!shlAutomationTool.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shlAutomationTool = new Shell();
		shlAutomationTool.addShellListener(new ShellAdapter() {
			@Override
			public void shellClosed(ShellEvent e) {
		        AppiumLogPrinter.close();
		        ThreadStop = true;
		        stopTest();
		        if(AppiumThread.isAlive())
				{
		        	ThreadStop = true;
				}
				try {
					Process proc = null;
					proc = Runtime.getRuntime().exec("killall -KILL node");
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		        
			}
		});
		
		String workingDir = System.getProperty("user.dir");
		String IconPath=workingDir+"/ford.ico";
		shlAutomationTool.setImage(SWTResourceManager.getImage(IconPath));
		shlAutomationTool.setSize(1157, 487);
		shlAutomationTool.setText("Automation Tool");
		
		Group groupAndroidCfg = new Group(shlAutomationTool, SWT.NONE);
		groupAndroidCfg.setBounds(10, 10, 405, 143);
		//groupAndroidCfg.setVisible(false);
		
		Label lblAndroidSerialNumber = new Label(groupAndroidCfg, SWT.NONE);
		lblAndroidSerialNumber.setBounds(10, 10, 129, 15);
		//lblAndroidSerialNumber.setText("Android Serial Number:");
		
		Label lblAppPath = new Label(groupAndroidCfg, SWT.NONE);
		lblAppPath.setBounds(10, 51, 55, 15);
		lblAppPath.setText("App Path:");
		
		textAppPath = new Text(groupAndroidCfg, SWT.BORDER);
		textAppPath.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				if(textAppPath.getText()!=null)
				{
					AppPath=textAppPath.getText();
					if(AppPath.indexOf(".apk")!=-1)
					{
						if(PhoneOS.equals("Android"))
						{
							String[] result=getAPKInfo(AppPath);
							AppPackage=result[0];
							AppActivity=result[1];
							m_AppPath = AppPath;
							textAppVersion.setText(AppSWVersion);
						}
					}
					if(AppPath.indexOf(".ipa")!=-1)
					{
						if(PhoneOS.equals("IOS"))
						{
							String result = getIpaInfo(AppPath);
							AppSWVersion = result;
							textAppVersion.setText(AppSWVersion);
						}
					}
				} 
			}
		});
		textAppPath.setBounds(71, 48, 267, 21);
		
		Button btnFindApp = new Button(groupAndroidCfg, SWT.NONE);
		btnFindApp.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String Path=getAppPathFromFileSystem();
				if(Path!=null)
				{
					textAppPath.setText(Path);
					textAppPath.setForeground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
				}
			}
		});
		btnFindApp.setBounds(344, 46, 39, 25);
		btnFindApp.setText("...");
		
		Label lblAppVersion = new Label(groupAndroidCfg, SWT.NONE);
		lblAppVersion.setBounds(10, 98, 73, 15);
		lblAppVersion.setText("App Version:");
		
		textAppVersion = new Text(groupAndroidCfg, SWT.BORDER);
		textAppVersion.setBounds(144, 95, 239, 21);
		
		Group groupJsonCfg = new Group(shlAutomationTool, SWT.NONE);
		groupJsonCfg.setText("App Object Config:");
		groupJsonCfg.setBounds(440, 10, 438, 143);
		
		textObjMapPath = new Text(groupJsonCfg, SWT.BORDER);
		textObjMapPath.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				if(textObjMapPath.getText()!=null)
				{
					OBJECT_MAP_PATH=textObjMapPath.getText();
					File inputWorkbook = new File(OBJECT_MAP_PATH);
					Workbook ConfigWb = null;
					try 
					{
						ConfigWb = Workbook.getWorkbook(inputWorkbook);
						String[] SheetName = ConfigWb.getSheetNames();
						if(SheetName!=null)
						{
							comboSheetName.setItems(SheetName);
							comboSheetName.setForeground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
						}
					} catch (BiffException | IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});
		textObjMapPath.setBounds(95, 7, 295, 21);
		
		Button buttonFindObjectMap = new Button(groupJsonCfg, SWT.NONE);
		buttonFindObjectMap.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String Path=getObjectPathFromFileSystem();
				if(Path!=null)
				{
					textObjMapPath.setText(Path);
					textObjMapPath.setForeground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
				}
			}
		});
		buttonFindObjectMap.setText("...");
		buttonFindObjectMap.setBounds(396, 5, 39, 25);
		
		Label lblObjMap = new Label(groupJsonCfg, SWT.NONE);
		lblObjMap.setBounds(10, 10, 79, 15);
		lblObjMap.setText("Obj Map Path:");
		
		Label lblObjectMapSheet = new Label(groupJsonCfg, SWT.NONE);
		lblObjectMapSheet.setBounds(10, 46, 132, 15);
		lblObjectMapSheet.setText("Object Map Sheet Name:");
		
		comboSheetName = new Combo(groupJsonCfg, SWT.NONE);
		comboSheetName.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				if(comboSheetName.getText()!=null)
				{
					OBJECT_SHEET_NAME=comboSheetName.getText();
				}
			}
		});
		comboSheetName.setBounds(148, 42, 103, 23);
		
		Label lblTestPlan = new Label(groupJsonCfg, SWT.NONE);
		lblTestPlan.setBounds(10, 86, 55, 15);
		lblTestPlan.setText("Test Plan:");
		
		textTestPlanPath = new Text(groupJsonCfg, SWT.BORDER);
		textTestPlanPath.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				if(textTestPlanPath.getText() != null)
				{
					TestPlanPath = textTestPlanPath.getText();
					System.out.println("Test plan Path = "+TestPlanPath);
				}
			}
		});
		textTestPlanPath.setBounds(94, 83, 295, 21);
		
		Button btnTestPlanPath = new Button(groupJsonCfg, SWT.NONE);
		btnTestPlanPath.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String Path=getTestPlanFromFileSystem();
				if(Path!=null)
				{
					textTestPlanPath.setText(Path);
					textTestPlanPath.setForeground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
				}
			}
		});
		btnTestPlanPath.setText("...");
		btnTestPlanPath.setBounds(396, 81, 39, 25);
		
		Label lbl_LogWindow = new Label(shlAutomationTool, SWT.NONE);
		lbl_LogWindow.setBounds(20, 159, 75, 15);
		lbl_LogWindow.setText("Log Window:");
		
		styledTextLog = new StyledText(shlAutomationTool, SWT.V_SCROLL |SWT.BORDER|SWT.H_SCROLL);
		styledTextLog.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				styledTextLog.setTopIndex(styledTextLog.getLineCount() - 1);
			}
			
		});
		styledTextLog.setText("                                                    Welcome to Use Automation Tool\r\n");
		styledTextLog.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.NORMAL));
		styledTextLog.setBounds(10, 180, 543, 259);
		styledTextLog.setVisible(true);
		styledTextLog.setBackground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
		styledTextLog.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		
		styledAppiumLog = new StyledText(shlAutomationTool, SWT.V_SCROLL | SWT.BORDER | SWT.H_SCROLL);
		styledAppiumLog.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				styledAppiumLog.setTopIndex(styledAppiumLog.getLineCount() - 1);
			}
			
		});
		styledAppiumLog.setVisible(true);
		styledAppiumLog.setText("                                                    Welcome to Use Automation Tool\r\n");
		styledAppiumLog.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		styledAppiumLog.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.NORMAL));
		styledAppiumLog.setBackground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
		styledAppiumLog.setBounds(580, 180, 556, 259);
		
		Group grpControlButton = new Group(shlAutomationTool, SWT.NONE);
		grpControlButton.setText("Control Button:");
		grpControlButton.setBounds(884, 10, 248, 63);
		
		Button btnNewButtonDone = new Button(grpControlButton, SWT.NONE);
		btnNewButtonDone.setBounds(129, 10, 95, 31);
		btnNewButtonDone.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				//Now Let use read all off the values and store them in
				// global values.
				collectCfgData();
				(new Thread(new Case())).start();
			}
		});
		btnNewButtonDone.setText("Start Test");
		
		Button btnStartAppium = new Button(grpControlButton, SWT.NONE);
		btnStartAppium.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(System.getProperty("os.name").indexOf("Mac") != -1)
				{
					OS = "MAC";
					ThreadStop = false;
					StartAppium();
				}
				else
				{
					OS = "WIN";
					StartAppium("LaunchAppium.bat");
				}
			}
		});
		btnStartAppium.setBounds(8, 10, 115, 31);
		btnStartAppium.setText("Start Appium");
		
		Label lblAppiumLogWindows = new Label(shlAutomationTool, SWT.NONE);
		lblAppiumLogWindows.setBounds(580, 159, 133, 15);
		lblAppiumLogWindows.setText("Appium Log Windows:");
		
		Group grpRunningTestCase = new Group(shlAutomationTool, SWT.NONE);
		grpRunningTestCase.setText("Running Test Case:");
		grpRunningTestCase.setBounds(884, 75, 248, 78);
		
		textGroup = new Text(grpRunningTestCase, SWT.BORDER);
		textGroup.setBounds(64, 10, 170, 19);
		
		textFeature = new Text(grpRunningTestCase, SWT.BORDER);
		textFeature.setBounds(64, 35, 170, 19);
		
		Label lblNewLabel = new Label(grpRunningTestCase, SWT.NONE);
		lblNewLabel.setBounds(10, 10, 40, 14);
		lblNewLabel.setText("Group:");
		
		Label lblFeature = new Label(grpRunningTestCase, SWT.NONE);
		lblFeature.setBounds(10, 38, 48, 14);
		lblFeature.setText("Feature:");
		
		setCfgData();
		
		if(PhoneOS != null)
		{
			if(PhoneOS.equals("Android"))
			{
				groupAndroidCfg.setText("Android Configuration");
				lblAndroidSerialNumber.setText("Android Serial Number:");
				comboDeviceID = new Combo(groupAndroidCfg, SWT.NONE);
				if(AndroidSerialNumber != null)
				{
					comboDeviceID.setText(AndroidSerialNumber);
				}
				comboDeviceID.addModifyListener(new ModifyListener() {
					public void modifyText(ModifyEvent arg0) {
						if(comboDeviceID.getText()!=null)
						{
							AndroidSerialNumber=comboDeviceID.getText();
							m_AndroidDevice = AndroidSerialNumber;
						}
					}
				});
				comboDeviceID.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseDown(MouseEvent e) {
						String[] AndroidSNArray=getSerialNumber();
						if(AndroidSNArray!=null)
						{
							comboDeviceID.setItems(AndroidSNArray);
							comboDeviceID.setForeground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
						}
						
					}
				});
				comboDeviceID.setBounds(145, 6, 238, 23);
			}
			else//(PhoneOS.equals("IOS"))
			{
				groupAndroidCfg.setText("IOS Configuration");
				lblAndroidSerialNumber.setText("IOS UDID:");
				comboDeviceID = new Combo(groupAndroidCfg, SWT.NONE);
				if(IOSUdid != null)
				{
					comboDeviceID.setText(IOSUdid);
				}
				comboDeviceID.addModifyListener(new ModifyListener() {
					public void modifyText(ModifyEvent arg0) {
						if(comboDeviceID.getText()!=null)
						{
							IOSUdid=comboDeviceID.getText();
						}
					}
				});
				comboDeviceID.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseDown(MouseEvent e) {
						String[] AndroidSNArray=getUdid();
						if(AndroidSNArray!=null)
						{
							comboDeviceID.setItems(AndroidSNArray);
							comboDeviceID.setForeground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
						}
						
					}
				});
				comboDeviceID.setBounds(145, 6, 238, 23);
				
			}
		}
	}
	
	/**
	 * Set the configuration data from cfg file onto GUI
	 */
	protected void setCfgData()
	{
		try 
		{
			String line = null;
			String fileName = System.getProperty("user.dir")+"/Cfg/Config.txt";
			File CfgFile = new File(fileName);
			if( CfgFile.exists())
			{
				FileReader fr = new FileReader(CfgFile);
	            BufferedReader br = new BufferedReader(fr);
	            while ((line = br.readLine()) != null) {
	                String[] Data=line.split("=");
	                switch(Data[0])
	                {
	                	case "PhoneOS":
	                	{
	                		PhoneOS = Data[1];
	                		break;
	                	}
	                	case "AndroidSerialNumber":
	                	{
	                		AndroidSerialNumber = Data[1];
	                		break;
	                	}
	                	case "IOSUdid":
	                	{
	                		IOSUdid = Data[1];
	                		break;
	                	}
	                	case "AppPath":
	                	{
	                		textAppPath.setText(Data[1]);
	                		break;
	                	}
	                	case "AppSWVersion":
	                	{
	                		textAppVersion.setText(Data[1]);
	                		break;
	                	}
	                	case "OBJECT_MAP_PATH":
	                	{
	                		textObjMapPath.setText(Data[1]);
	                		break;
	                	}
	                	case "OBJECT_SHEET_NAME":
	                	{
	                		comboSheetName.setText(Data[1]);
	                		break;
	                	}
	                	case "TestPlanPath":
	                	{
	                		textTestPlanPath.setText(Data[1]);
	                	}
	                	default:
	                		System.out.println("The configuration file not correct!");
	                }
	            }
	            
	            fr.close();
	            br.close();
			}
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	/**
	 * This function is used to collect the configuration data from the GUI
	 */
	protected void collectCfgData()
	{
		try {
			// Save the programmer logging file
			File dir = new File("Cfg");
			if(dir.exists() == false)
				dir.mkdir();
			String fileName = System.getProperty("user.dir")+"/Cfg/Config.txt";
			File LogFile=new File(fileName);
			if(LogFile.exists() == false)
			{
				LogFile.createNewFile();
			}
			FileWriter fw;
			fw = new FileWriter(LogFile);
		    BufferedWriter out = new BufferedWriter(fw);
		    if(PhoneOS != null)
		    {
		    	out.write("PhoneOS="+PhoneOS);
                out.newLine();
		    }
	        if(AndroidSerialNumber!=null)
	        {
                out.write("AndroidSerialNumber="+AndroidSerialNumber);
                out.newLine();
	        }
	        if(IOSUdid!=null)
	        {
                out.write("IOSUdid="+IOSUdid);
                out.newLine();
	        }
	        if(AppPath!=null)
	        {
	        	out.write("AppPath="+AppPath);
	        	out.newLine();
	        }
	        if(AppSWVersion!=null)
	        {
	        	out.write("AppSWVersion="+AppSWVersion);
	        	out.newLine();
	        }
	        if(OBJECT_MAP_PATH!=null)
	        {
	        	out.write("OBJECT_MAP_PATH="+OBJECT_MAP_PATH);
	        	out.newLine();
	        }
	        if(OBJECT_SHEET_NAME!=null)
	        {
	        	out.write("OBJECT_SHEET_NAME="+OBJECT_SHEET_NAME);
	        	out.newLine();
	        }
	        if(TestPlanPath != null)
	        {
	        	out.write("TestPlanPath="+TestPlanPath);
	        	out.newLine();
	        }
	        out.flush();
	        out.close();
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	/**
	 * Get the App path
	 */
	protected String getAppPathFromFileSystem()
	{
		FileDialog fd = new FileDialog(shlAutomationTool, SWT.OPEN);
        fd.setText("Open");
        fd.setFilterPath("C:/");
        if(PhoneOS.equals("Android"))
        {
        	String[] filterExt = { "*.apk"};
        	fd.setFilterExtensions(filterExt);
        }
        if(PhoneOS.equals("IOS"))
        {
        	String[] filterExt = { "*.ipa" };
        	fd.setFilterExtensions(filterExt);
        }
        String selected = fd.open();
        return selected;
	}
	
	/**
	 * Get the object map files path
	 */
	protected String getObjectPathFromFileSystem()
	{
		FileDialog fd = new FileDialog(shlAutomationTool, SWT.OPEN);
        fd.setText("Open");
        fd.setFilterPath("C:/");
        String[] filterExt = { "*.xls"};
        fd.setFilterExtensions(filterExt);
        String selected = fd.open();
        return selected;
	}
	
	/**
	 * Get the test plan excel path
	 */
	protected String getTestPlanFromFileSystem()
	{
		FileDialog fd = new FileDialog(shlAutomationTool, SWT.OPEN);
        fd.setText("Open");
        fd.setFilterPath("C:/");
        String[] filterExt = { "*.xls"};
        fd.setFilterExtensions(filterExt);
        String selected = fd.open();
        return selected;
	}
}

class exitEvent implements Runnable 
{
	public static void test ()
	  {
		int choice = JOptionPane.showConfirmDialog(null,"Do you want save the configuration information",
           		"Select answer",
           		JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE);
		//System.out.println("The choice is:"+choice);
		if(choice==0)
		{
			
			System.out.print("Yes chosed!");
		}
		System.exit(0);
	  }

	@Override
	public void run() {
		// TODO Auto-generated method stub
		test();
	}
}
#!/usr/bin/env bash
echo Hello 
echo $PATH
cd /bin
export PATH=${PATH}:/Users/Automation/Android/SDK/tools
export ANDROID_HOME=/Users/Automation/Android/SDK
export PATH=${PATH}:/Applications/Appium.app/Contents/Resources/node/bin:/usr/local/Cellar/ideviceinstaller/1.1.0_2/bin:/usr/local/Cellar/libimobiledevice/1.2.0/bin
source ~/.bash_profile
killall -KILL node
/usr/local/bin/appium  -a 127.0.0.1  -p 4723  --platform-name=Android --platform-version=23 --automation-name=Appium --log-no-color --log-no-color